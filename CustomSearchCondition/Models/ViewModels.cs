﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Script.Serialization;

namespace CustomSearchCondition.Models
{
    public class ConditionViewModel
    {
        /// <summary>
        /// 关系（并且、或者）
        /// </summary>
        public string Relation { get; set; }

        /// <summary>
        /// 字段名
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// 操作符可包含类型
        /// </summary>
        public string Operation { get; set; }

        /// <summary>
        /// 输入值
        /// </summary>
        public string InputValue { get; set; }
    }

    public class ManResQueryCompareFieldInfo
    {
        /// <summary>
        /// 前台对比字段
        /// </summary>
        public String FieldName { get; set; }

        /// <summary>
        /// 数据库字段名
        /// </summary>
        public String DbFieldName { get; set; }

        /// <summary>
        /// 数据库表名
        /// </summary>
        public String TableName { get; set; }

    }

    public class UserInfoViewModel
    {
        /// <summary>
        /// 员工ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 员工姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 工号
        /// </summary>
        public string JobNumber { get; set; }

        /// <summary>
        /// 职位
        /// </summary>
        public string JobPosition { get; set; }

        /// <summary>
        /// 省级ID
        /// </summary>
        public string ProvinceName { get; set; }

        /// <summary>
        /// 市级ID
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        [ScriptIgnore]
        public DateTime Birthday { get; set; }

        public string BirthdayStr
        {
            get { return Birthday.ToString("yyyy-MM-dd"); }
        }
    }
}