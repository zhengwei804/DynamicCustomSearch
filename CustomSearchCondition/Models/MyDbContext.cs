﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CustomSearchCondition.Models
{
    public class MyDbContext : DbContext
    {
        public MyDbContext() : base("name=DefaultConnection")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntity>().ToTable("sys_user");
            modelBuilder.Entity<AreaEntity>().ToTable("sys_area");
            modelBuilder.Entity<JobPositionEntity>().ToTable("sys_job_position");
            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<UserEntity> Users { get; set; }

        public virtual DbSet<AreaEntity> Areas { get; set; }

        public virtual DbSet<JobPositionEntity> JobPositions { get; set; }
    }

    #region 实体类

    public class UserEntity
    {
        /// <summary>
        /// 员工ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 员工姓名
        /// </summary>
        [StringLength(20)]
        [Index("IX_RealName_JobNumber", Order = 1)]
        public string RealName { get; set; }

        /// <summary>
        /// 工号
        /// </summary>
        [StringLength(20)]
        [Index("IX_RealName_JobNumber", Order = 2)]
        public string JobNumber { get; set; }

        /// <summary>
        /// 职位
        /// </summary>
        public int JobPosition { get; set; }

        /// <summary>
        /// 省级ID
        /// </summary>
        [StringLength(20)]
        public string ProvinceID { get; set; }

        /// <summary>
        /// 市级ID
        /// </summary>
        [StringLength(20)]
        public string CityID { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        [Column(TypeName = "date")]
        public DateTime Birthday { get; set; }
    }

    /// <summary>
    /// 区域信息（基础信息、省市区）
    /// </summary>
    public class AreaEntity
    {
        /// <summary>
        /// 区域ID
        /// </summary>
        [Key]
        [StringLength(10)]
        public string ID { get; set; }

        /// <summary>
        /// 区域名称
        /// </summary>
        [StringLength(20)]
        public string AreaName { get; set; }

        /// <summary>
        /// 父级ID
        /// </summary>
        public string ParentID { get; set; }

    }

    public class JobPositionEntity
    {
        /// <summary>
        /// 职位ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 职位名称
        /// </summary>
        [StringLength(50)]
        public string PositionName { get; set; }
    }

    #endregion
}