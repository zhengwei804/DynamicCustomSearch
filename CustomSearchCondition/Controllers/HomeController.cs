﻿using CustomSearchCondition.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace CustomSearchCondition.Controllers
{
    public class HomeController : Controller
    {
        private List<ManResQueryCompareFieldInfo> CompareFieldInfoList = new List<ManResQueryCompareFieldInfo>
            {
                new ManResQueryCompareFieldInfo{ FieldName="员工姓名", DbFieldName="RealName", },
                new ManResQueryCompareFieldInfo{ FieldName="年龄", DbFieldName="Age", },
                new ManResQueryCompareFieldInfo{ FieldName="出生日期", DbFieldName="Birthday", },
                new ManResQueryCompareFieldInfo{ FieldName="工号", DbFieldName="JobNumber", },
                new ManResQueryCompareFieldInfo{ FieldName="职位", DbFieldName="JobPosition", },
                new ManResQueryCompareFieldInfo{ FieldName="籍贯", DbFieldName="ProvinceID", },

            };
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Search(List<ConditionViewModel> conditions)
        {
            var UserExpress = ExpressionExtension.True<UserEntity>();
            //上下语句作用相同
            //Expression<Func<UserEntity, bool>> express = (UserEntity) => true;
            if (conditions != null)
            {
                //循环查询条件
                foreach (var item in conditions)
                {
                    var fieldInfo = CompareFieldInfoList.Where(t => t.FieldName.Equals(item.Field)).FirstOrDefault();
                    if (fieldInfo != null)
                    {
                        var expression = UserExpress.BuildDynamicExpression(fieldInfo.DbFieldName, item.Operation, item.InputValue);
                        if (item.Relation.Equals("and"))
                        {
                            UserExpress = UserExpress.And(expression);
                        }
                        else if (item.Relation.Equals("or"))
                        {
                            UserExpress = UserExpress.Or(expression);
                        }
                    }
                }
            }
            using (var db = new MyDbContext())
            {
                var query = from user in db.Users.Where(UserExpress)
                            join city in db.Areas on user.CityID equals city.ID
                            join province in db.Areas on city.ParentID equals province.ID
                            join jobposition in db.JobPositions on user.JobPosition equals jobposition.ID
                            select new UserInfoViewModel
                            {
                                ID = user.ID,
                                RealName = user.RealName,
                                Birthday = user.Birthday,
                                Age = user.Age,
                                JobNumber = user.JobNumber,
                                ProvinceName = province.AreaName,
                                CityName = city.AreaName,
                                JobPosition = jobposition.PositionName,
                            };
                var userlist = query.ToList();
                return Json(userlist, JsonRequestBehavior.AllowGet);

            }
        }
    }
}