namespace CustomSearchCondition.Migrations
{
    using CustomSearchCondition.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CustomSearchCondition.Models.MyDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(CustomSearchCondition.Models.MyDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            #region ����

            var areas = new List<AreaEntity>
            {
                new AreaEntity { ID = "320000", AreaName = "����ʡ" , ParentID = "0" },
                new AreaEntity { ID = "340000", AreaName = "����ʡ" , ParentID = "0" },
                new AreaEntity { ID = "320100", AreaName = "�Ͼ���" , ParentID = "320000" },
                new AreaEntity { ID = "320200", AreaName = "������" , ParentID = "320000" },
                new AreaEntity { ID = "320300", AreaName = "������" , ParentID = "320000" },
                new AreaEntity { ID = "320400", AreaName = "������" , ParentID = "320000" },
                new AreaEntity { ID = "320500", AreaName = "������" , ParentID = "320000" },
                new AreaEntity { ID = "320600", AreaName = "��ͨ��" , ParentID = "320000" },
                new AreaEntity { ID = "320700", AreaName = "���Ƹ���" , ParentID = "320000" },
                new AreaEntity { ID = "320800", AreaName = "������" , ParentID = "320000" },
                new AreaEntity { ID = "320900", AreaName = "�γ���" , ParentID = "320000" },
                new AreaEntity { ID = "321000", AreaName = "������" , ParentID = "320000" },
                new AreaEntity { ID = "321100", AreaName = "����" , ParentID = "320000" },
                new AreaEntity { ID = "321200", AreaName = "̩����" , ParentID = "320000" },
                new AreaEntity { ID = "321300", AreaName = "��Ǩ��" , ParentID = "320000" },
                new AreaEntity { ID = "340100", AreaName = "�Ϸ���" , ParentID = "340000" },
                new AreaEntity { ID = "340200", AreaName = "�ߺ���" , ParentID = "340000" },
                new AreaEntity { ID = "340300", AreaName = "������" , ParentID = "340000" },
                new AreaEntity { ID = "340400", AreaName = "������" , ParentID = "340000" },
                new AreaEntity { ID = "340500", AreaName = "����ɽ��" , ParentID = "340000" },
                new AreaEntity { ID = "340600", AreaName = "������" , ParentID = "340000" },
                new AreaEntity { ID = "340700", AreaName = "ͭ����" , ParentID = "340000" },
                new AreaEntity { ID = "340800", AreaName = "������" , ParentID = "340000" },
                new AreaEntity { ID = "341000", AreaName = "��ɽ��" , ParentID = "340000" },
                new AreaEntity { ID = "341100", AreaName = "������" , ParentID = "340000" },
                new AreaEntity { ID = "341200", AreaName = "������" , ParentID = "340000" },
                new AreaEntity { ID = "341300", AreaName = "������" , ParentID = "340000" },
                new AreaEntity { ID = "341500", AreaName = "������" , ParentID = "340000" },
                new AreaEntity { ID = "341600", AreaName = "������" , ParentID = "340000" },
                new AreaEntity { ID = "341700", AreaName = "������" , ParentID = "340000" },
                new AreaEntity { ID = "341800", AreaName = "������" , ParentID = "340000" },
            };

            context.Areas.AddOrUpdate(areas.ToArray());

            #endregion

            #region ְλ

            var jobpositions = new List<JobPositionEntity> {
                new JobPositionEntity { ID = 1 , PositionName="�伦ʪ" },
                new JobPositionEntity { ID = 2 , PositionName="����ʦ" },
                new JobPositionEntity { ID = 3 , PositionName="��ũ" },
                new JobPositionEntity { ID = 4 , PositionName="����" },
                new JobPositionEntity { ID = 5 , PositionName="��ū" },

            };
            context.JobPositions.AddOrUpdate(jobpositions.ToArray());

            #endregion

            #region Ա��

            var users = new List<UserEntity>
            {
                new UserEntity{ JobNumber="048478", Birthday=Convert.ToDateTime("1993-06-13"), Age= 24 , ProvinceID="320000", CityID="320200", JobPosition = 1, RealName="������" },
                new UserEntity{ JobNumber="004913", Birthday=Convert.ToDateTime("1992-02-16"), Age= 25 , ProvinceID="320000", CityID="320200", JobPosition = 1, RealName="��ӱ��" },
                new UserEntity{ JobNumber="020877", Birthday=Convert.ToDateTime("1992-11-12"), Age= 24 , ProvinceID="320000", CityID="320200", JobPosition = 1, RealName="֣����" },
                new UserEntity{ JobNumber="008370", Birthday=Convert.ToDateTime("1986-09-24"), Age= 31 , ProvinceID="320000", CityID="320200", JobPosition = 1, RealName="����" },
                new UserEntity{ JobNumber="014491", Birthday=Convert.ToDateTime("1983-12-29"), Age= 33 , ProvinceID="320000", CityID="320200", JobPosition = 1, RealName="��ǿ" },
                new UserEntity{ JobNumber="015602", Birthday=Convert.ToDateTime("1991-04-29"), Age= 26 , ProvinceID="320000", CityID="320300", JobPosition = 1, RealName="������" },
                new UserEntity{ JobNumber="005103", Birthday=Convert.ToDateTime("1993-08-16"), Age= 24 , ProvinceID="320000", CityID="320300", JobPosition = 1, RealName="��Ӣ" },
                new UserEntity{ JobNumber="006791", Birthday=Convert.ToDateTime("1996-09-19"), Age= 21 , ProvinceID="320000", CityID="320300", JobPosition = 1, RealName="�����" },
                new UserEntity{ JobNumber="013183", Birthday=Convert.ToDateTime("1992-12-27"), Age= 24 , ProvinceID="320000", CityID="320200", JobPosition = 1, RealName="��ΰ" },
                new UserEntity{ JobNumber="010550", Birthday=Convert.ToDateTime("1985-12-01"), Age= 31 , ProvinceID="320000", CityID="320200", JobPosition = 1, RealName="����" },
                new UserEntity{ JobNumber="000681", Birthday=Convert.ToDateTime("1987-12-12"), Age= 29 , ProvinceID="320000", CityID="320200", JobPosition = 2, RealName="����" },
                new UserEntity{ JobNumber="020854", Birthday=Convert.ToDateTime("1989-10-07"), Age= 28 , ProvinceID="320000", CityID="320200", JobPosition = 2, RealName="��С��" },
                new UserEntity{ JobNumber="048216", Birthday=Convert.ToDateTime("1992-06-21"), Age= 25 , ProvinceID="320000", CityID="320200", JobPosition = 2, RealName="����" },
                new UserEntity{ JobNumber="007016", Birthday=Convert.ToDateTime("1993-04-14"), Age= 24 , ProvinceID="320000", CityID="320100", JobPosition = 2, RealName="�ź���" },
                new UserEntity{ JobNumber="009109", Birthday=Convert.ToDateTime("1987-04-25"), Age= 30 , ProvinceID="320000", CityID="320100", JobPosition = 2, RealName="Ҷ��Ӣ" },
                new UserEntity{ JobNumber="016148", Birthday=Convert.ToDateTime("1989-06-01"), Age= 28 , ProvinceID="320000", CityID="320100", JobPosition = 2, RealName="������" },
                new UserEntity{ JobNumber="000620", Birthday=Convert.ToDateTime("1981-07-04"), Age= 36 , ProvinceID="320000", CityID="320100", JobPosition = 2, RealName="��ջ�" },
                new UserEntity{ JobNumber="021190", Birthday=Convert.ToDateTime("1982-08-20"), Age= 35 , ProvinceID="320000", CityID="320100", JobPosition = 2, RealName="����ѧ" },
                new UserEntity{ JobNumber="014557", Birthday=Convert.ToDateTime("1990-09-27"), Age= 27 , ProvinceID="320000", CityID="320800", JobPosition = 3, RealName="������" },
                new UserEntity{ JobNumber="011933", Birthday=Convert.ToDateTime("1989-04-11"), Age= 28 , ProvinceID="320000", CityID="320800", JobPosition = 3, RealName="��С��" },
                new UserEntity{ JobNumber="019997", Birthday=Convert.ToDateTime("1988-10-20"), Age= 28 , ProvinceID="320000", CityID="320800", JobPosition = 3, RealName="Ƚ��" },
                new UserEntity{ JobNumber="017212", Birthday=Convert.ToDateTime("1991-02-27"), Age= 26 , ProvinceID="320000", CityID="320800", JobPosition = 3, RealName="�ֹ�Ӣ" },
                new UserEntity{ JobNumber="016463", Birthday=Convert.ToDateTime("1994-04-02"), Age= 23 , ProvinceID="320000", CityID="320800", JobPosition = 3, RealName="����" },
                new UserEntity{ JobNumber="006765", Birthday=Convert.ToDateTime("1993-05-08"), Age= 24 , ProvinceID="320000", CityID="320500", JobPosition = 3, RealName="�ų���" },
                new UserEntity{ JobNumber="020778", Birthday=Convert.ToDateTime("1994-05-27"), Age= 23 , ProvinceID="320000", CityID="320500", JobPosition = 3, RealName="�ܷ���" },
                new UserEntity{ JobNumber="004067", Birthday=Convert.ToDateTime("1990-01-07"), Age= 27 , ProvinceID="320000", CityID="320500", JobPosition = 3, RealName="����" },
                new UserEntity{ JobNumber="014278", Birthday=Convert.ToDateTime("1980-06-12"), Age= 37 , ProvinceID="320000", CityID="320500", JobPosition = 3, RealName="�����" },
                new UserEntity{ JobNumber="018095", Birthday=Convert.ToDateTime("1993-07-21"), Age= 24 , ProvinceID="320000", CityID="320500", JobPosition = 3, RealName="������" },
                new UserEntity{ JobNumber="014584", Birthday=Convert.ToDateTime("1989-09-06"), Age= 28 , ProvinceID="320000", CityID="320500", JobPosition = 3, RealName="��ٻ" },
                new UserEntity{ JobNumber="001224", Birthday=Convert.ToDateTime("1990-05-03"), Age= 27 , ProvinceID="320000", CityID="320500", JobPosition = 3, RealName="����" },
                new UserEntity{ JobNumber="010835", Birthday=Convert.ToDateTime("1995-08-17"), Age= 22 , ProvinceID="320000", CityID="320500", JobPosition = 3, RealName="������" },
                new UserEntity{ JobNumber="020293", Birthday=Convert.ToDateTime("1988-12-18"), Age= 28 , ProvinceID="320000", CityID="320500", JobPosition = 3, RealName="������" },
                new UserEntity{ JobNumber="005065", Birthday=Convert.ToDateTime("1992-02-23"), Age= 25 , ProvinceID="320000", CityID="320500", JobPosition = 3, RealName="����" },
                new UserEntity{ JobNumber="005091", Birthday=Convert.ToDateTime("1997-06-25"), Age= 20 , ProvinceID="340000", CityID="341300", JobPosition = 3, RealName="������" },
                new UserEntity{ JobNumber="001473", Birthday=Convert.ToDateTime("1979-08-11"), Age= 38 , ProvinceID="340000", CityID="341300", JobPosition = 3, RealName="����֥" },
                new UserEntity{ JobNumber="021375", Birthday=Convert.ToDateTime("1987-04-13"), Age= 30 , ProvinceID="340000", CityID="341300", JobPosition = 4, RealName="Ԭ����" },
                new UserEntity{ JobNumber="022163", Birthday=Convert.ToDateTime("1994-08-12"), Age= 23 , ProvinceID="340000", CityID="341300", JobPosition = 4, RealName="Ф��" },
                new UserEntity{ JobNumber="047020", Birthday=Convert.ToDateTime("1967-11-24"), Age= 49 , ProvinceID="340000", CityID="341300", JobPosition = 4, RealName="�պ���" },
                new UserEntity{ JobNumber="022377", Birthday=Convert.ToDateTime("1993-10-20"), Age= 23 , ProvinceID="340000", CityID="341300", JobPosition = 4, RealName="����" },
                new UserEntity{ JobNumber="021168", Birthday=Convert.ToDateTime("1995-04-18"), Age= 22 , ProvinceID="340000", CityID="341300", JobPosition = 4, RealName="����" },
                new UserEntity{ JobNumber="014005", Birthday=Convert.ToDateTime("1992-12-16"), Age= 24 , ProvinceID="340000", CityID="341300", JobPosition = 4, RealName="�ྲ" },
                new UserEntity{ JobNumber="013609", Birthday=Convert.ToDateTime("1988-12-25"), Age= 28 , ProvinceID="340000", CityID="341300", JobPosition = 4, RealName="����֥" },
                new UserEntity{ JobNumber="004861", Birthday=Convert.ToDateTime("1985-07-13"), Age= 32 , ProvinceID="340000", CityID="341300", JobPosition = 4, RealName="�Ű���" },
                new UserEntity{ JobNumber="012329", Birthday=Convert.ToDateTime("1991-07-18"), Age= 26 , ProvinceID="340000", CityID="340100", JobPosition = 4, RealName="ʷ����" },
                new UserEntity{ JobNumber="046206", Birthday=Convert.ToDateTime("1991-06-05"), Age= 26 , ProvinceID="340000", CityID="340100", JobPosition = 4, RealName="��ͤ��" },
                new UserEntity{ JobNumber="003836", Birthday=Convert.ToDateTime("1972-03-25"), Age= 45 , ProvinceID="340000", CityID="340100", JobPosition = 4, RealName="���׷�" },
                new UserEntity{ JobNumber="020727", Birthday=Convert.ToDateTime("1989-10-01"), Age= 28 , ProvinceID="340000", CityID="340100", JobPosition = 4, RealName="������" },
                new UserEntity{ JobNumber="020174", Birthday=Convert.ToDateTime("1991-03-02"), Age= 26 , ProvinceID="340000", CityID="340100", JobPosition = 4, RealName="���Ǿ�" },
                new UserEntity{ JobNumber="003899", Birthday=Convert.ToDateTime("1984-04-05"), Age= 33 , ProvinceID="340000", CityID="340100", JobPosition = 4, RealName="����" },
                new UserEntity{ JobNumber="010809", Birthday=Convert.ToDateTime("1986-12-27"), Age= 30 , ProvinceID="340000", CityID="340100", JobPosition = 4, RealName="������" },
                new UserEntity{ JobNumber="012502", Birthday=Convert.ToDateTime("1992-02-29"), Age= 25 , ProvinceID="340000", CityID="340100", JobPosition = 4, RealName="�⾲" },
                new UserEntity{ JobNumber="019604", Birthday=Convert.ToDateTime("1992-10-14"), Age= 24 , ProvinceID="340000", CityID="340100", JobPosition = 4, RealName="��˼��" },
                new UserEntity{ JobNumber="020348", Birthday=Convert.ToDateTime("1987-09-02"), Age= 30 , ProvinceID="340000", CityID="340100", JobPosition = 4, RealName="������" },
                new UserEntity{ JobNumber="018874", Birthday=Convert.ToDateTime("1994-10-22"), Age= 22 , ProvinceID="340000", CityID="340100", JobPosition = 4, RealName="���" },
                new UserEntity{ JobNumber="004128", Birthday=Convert.ToDateTime("1991-11-24"), Age= 25 , ProvinceID="340000", CityID="340400", JobPosition = 4, RealName="���" },
                new UserEntity{ JobNumber="002438", Birthday=Convert.ToDateTime("1995-06-01"), Age= 22 , ProvinceID="340000", CityID="340400", JobPosition = 5, RealName="��˼" },
                new UserEntity{ JobNumber="018290", Birthday=Convert.ToDateTime("1996-07-25"), Age= 21 , ProvinceID="340000", CityID="340400", JobPosition = 5, RealName="����" },
                new UserEntity{ JobNumber="000092", Birthday=Convert.ToDateTime("1975-09-11"), Age= 42 , ProvinceID="340000", CityID="340400", JobPosition = 5, RealName="��ʱƽ" },
                new UserEntity{ JobNumber="000695", Birthday=Convert.ToDateTime("1990-10-29"), Age= 26 , ProvinceID="340000", CityID="340400", JobPosition = 5, RealName="����" },
                new UserEntity{ JobNumber="010451", Birthday=Convert.ToDateTime("1990-01-08"), Age= 27 , ProvinceID="340000", CityID="340400", JobPosition = 5, RealName="������" },
                new UserEntity{ JobNumber="013640", Birthday=Convert.ToDateTime("1995-04-17"), Age= 22 , ProvinceID="340000", CityID="340400", JobPosition = 5, RealName="����" },
                new UserEntity{ JobNumber="000504", Birthday=Convert.ToDateTime("1975-06-08"), Age= 42 , ProvinceID="340000", CityID="340400", JobPosition = 5, RealName="�±���" },
                new UserEntity{ JobNumber="048440", Birthday=Convert.ToDateTime("1995-07-02"), Age= 22 , ProvinceID="340000", CityID="340400", JobPosition = 5, RealName="����" },
                new UserEntity{ JobNumber="005357", Birthday=Convert.ToDateTime("1994-03-24"), Age= 23 , ProvinceID="340000", CityID="340400", JobPosition = 5, RealName="�ű�֣" },
                new UserEntity{ JobNumber="001279", Birthday=Convert.ToDateTime("1989-03-16"), Age= 28 , ProvinceID="340000", CityID="340400", JobPosition = 5, RealName="�ƽ�" },
                new UserEntity{ JobNumber="019167", Birthday=Convert.ToDateTime("1993-01-25"), Age= 24 , ProvinceID="340000", CityID="340400", JobPosition = 5, RealName="����" },
                new UserEntity{ JobNumber="020781", Birthday=Convert.ToDateTime("1987-02-02"), Age= 30 , ProvinceID="340000", CityID="341800", JobPosition = 5, RealName="л����" },
                new UserEntity{ JobNumber="046376", Birthday=Convert.ToDateTime("1981-02-09"), Age= 36 , ProvinceID="340000", CityID="341800", JobPosition = 5, RealName="������" },
                new UserEntity{ JobNumber="008851", Birthday=Convert.ToDateTime("1987-09-04"), Age= 30 , ProvinceID="340000", CityID="341800", JobPosition = 5, RealName="����־" },
                new UserEntity{ JobNumber="010082", Birthday=Convert.ToDateTime("1995-08-23"), Age= 22 , ProvinceID="340000", CityID="341800", JobPosition = 5, RealName="Ф����" },
                new UserEntity{ JobNumber="001306", Birthday=Convert.ToDateTime("1989-10-10"), Age= 28 , ProvinceID="340000", CityID="341800", JobPosition = 5, RealName="������" },
                new UserEntity{ JobNumber="010883", Birthday=Convert.ToDateTime("1997-01-21"), Age= 20 , ProvinceID="340000", CityID="341800", JobPosition = 5, RealName="ʥѩ��" },
                new UserEntity{ JobNumber="007941", Birthday=Convert.ToDateTime("1986-05-14"), Age= 31 , ProvinceID="340000", CityID="341800", JobPosition = 1, RealName="л��" },
                new UserEntity{ JobNumber="008654", Birthday=Convert.ToDateTime("1990-06-14"), Age= 27 , ProvinceID="340000", CityID="341800", JobPosition = 1, RealName="�ս���" },
                new UserEntity{ JobNumber="008425", Birthday=Convert.ToDateTime("1984-11-28"), Age= 32 , ProvinceID="340000", CityID="341800", JobPosition = 1, RealName="ׯ����" },
                new UserEntity{ JobNumber="001319", Birthday=Convert.ToDateTime("1984-11-25"), Age= 32 , ProvinceID="340000", CityID="341800", JobPosition = 1, RealName="���߶�" },
                new UserEntity{ JobNumber="013362", Birthday=Convert.ToDateTime("1990-09-17"), Age= 27 , ProvinceID="340000", CityID="341800", JobPosition = 1, RealName="����" },
                new UserEntity{ JobNumber="019966", Birthday=Convert.ToDateTime("1990-08-25"), Age= 27 , ProvinceID="340000", CityID="341800", JobPosition = 1, RealName="�Գ�" },
                new UserEntity{ JobNumber="020770", Birthday=Convert.ToDateTime("1991-08-20"), Age= 26 , ProvinceID="340000", CityID="341800", JobPosition = 1, RealName="������" },
                new UserEntity{ JobNumber="004728", Birthday=Convert.ToDateTime("1988-05-08"), Age= 29 , ProvinceID="340000", CityID="341600", JobPosition = 2, RealName="����" },
                new UserEntity{ JobNumber="014349", Birthday=Convert.ToDateTime("1988-02-01"), Age= 29 , ProvinceID="340000", CityID="341600", JobPosition = 2, RealName="������" },
                new UserEntity{ JobNumber="046348", Birthday=Convert.ToDateTime("1978-06-28"), Age= 39 , ProvinceID="340000", CityID="341600", JobPosition = 2, RealName="����" },
                new UserEntity{ JobNumber="004914", Birthday=Convert.ToDateTime("1984-06-11"), Age= 33 , ProvinceID="340000", CityID="341600", JobPosition = 2, RealName="����" },
                new UserEntity{ JobNumber="011843", Birthday=Convert.ToDateTime("1981-08-19"), Age= 36 , ProvinceID="340000", CityID="341600", JobPosition = 2, RealName="��" },
                new UserEntity{ JobNumber="048330", Birthday=Convert.ToDateTime("1989-12-01"), Age= 27 , ProvinceID="340000", CityID="341600", JobPosition = 2, RealName="л����" },
                new UserEntity{ JobNumber="015049", Birthday=Convert.ToDateTime("1996-09-18"), Age= 21 , ProvinceID="340000", CityID="341600", JobPosition = 2, RealName="�����" },
                new UserEntity{ JobNumber="018897", Birthday=Convert.ToDateTime("1999-11-22"), Age= 17 , ProvinceID="340000", CityID="341600", JobPosition = 2, RealName="�����" },
                new UserEntity{ JobNumber="009362", Birthday=Convert.ToDateTime("1993-04-10"), Age= 24 , ProvinceID="340000", CityID="341600", JobPosition = 2, RealName="��ʥ" },
                new UserEntity{ JobNumber="004034", Birthday=Convert.ToDateTime("1988-03-05"), Age= 29 , ProvinceID="340000", CityID="341600", JobPosition = 2, RealName="����" },
                new UserEntity{ JobNumber="019204", Birthday=Convert.ToDateTime("1993-02-28"), Age= 24 , ProvinceID="340000", CityID="341600", JobPosition = 1, RealName="�����" },
                new UserEntity{ JobNumber="000091", Birthday=Convert.ToDateTime("1975-08-06"), Age= 42 , ProvinceID="340000", CityID="340500", JobPosition = 1, RealName="������" },
                new UserEntity{ JobNumber="003591", Birthday=Convert.ToDateTime("1974-01-24"), Age= 43 , ProvinceID="340000", CityID="340500", JobPosition = 3, RealName="���ѻ�" },
                new UserEntity{ JobNumber="008585", Birthday=Convert.ToDateTime("1994-08-27"), Age= 23 , ProvinceID="340000", CityID="340500", JobPosition = 3, RealName="�����" },
                new UserEntity{ JobNumber="014006", Birthday=Convert.ToDateTime("1992-04-24"), Age= 25 , ProvinceID="340000", CityID="340500", JobPosition = 3, RealName="��¶" },
                new UserEntity{ JobNumber="012624", Birthday=Convert.ToDateTime("1992-08-24"), Age= 25 , ProvinceID="340000", CityID="340500", JobPosition = 3, RealName="����" },
                new UserEntity{ JobNumber="019347", Birthday=Convert.ToDateTime("1999-07-08"), Age= 18 , ProvinceID="340000", CityID="340500", JobPosition = 3, RealName="Фǫ" },
                new UserEntity{ JobNumber="003658", Birthday=Convert.ToDateTime("1983-10-20"), Age= 33 , ProvinceID="340000", CityID="340500", JobPosition = 3, RealName="������" },
                new UserEntity{ JobNumber="009151", Birthday=Convert.ToDateTime("1994-10-03"), Age= 23 , ProvinceID="340000", CityID="340500", JobPosition = 3, RealName="������" },
                new UserEntity{ JobNumber="008256", Birthday=Convert.ToDateTime("1987-06-15"), Age= 30 , ProvinceID="340000", CityID="340500", JobPosition = 3, RealName="����" },
                new UserEntity{ JobNumber="004628", Birthday=Convert.ToDateTime("1979-12-04"), Age= 37 , ProvinceID="340000", CityID="340500", JobPosition = 3, RealName="������" },
            };

            context.Users.AddOrUpdate(t => t.JobNumber, users.ToArray());

            #endregion
        }
    }
}
